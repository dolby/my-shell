FROM python:3.9-slim-buster

ARG CLI_VERSION=2.13.0
ARG KUBE_VERSION=1.24.15

RUN apt-get update -qq && \
  apt-get install -qqy curl unzip vim-nox

RUN curl -s "https://awscli.amazonaws.com/awscli-exe-linux-x86_64-${CLI_VERSION}.zip" -o "awscliv2.zip" && \
  unzip awscliv2.zip && \
  ./aws/install && \
  rm -fr ./aws

RUN curl -L -o kubectl https://dl.k8s.io/v${KUBE_VERSION}/bin/linux/amd64/kubectl; \
    chmod +x kubectl; \
    mv kubectl /usr/local/bin

CMD bash
